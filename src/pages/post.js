// @flow
import React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import queryString from 'query-string';
import { Post } from '../libs/types';
import { getPostByIdService } from '../services/post';
import MyEditor from '../components/Editor';
import Ripple from '../components/Ripple';
import Container from '../components/Container';
import Content from '../components/Content';
import styled from 'styled-components';
type Props = {};
type State = {
  post: Post | null
};

export default class PostPage extends React.Component<
  Props & RouteComponentProps,
  State
> {
  state = {
    post: null
  };

  async componentDidMount() {
    try {
      const parsed = queryString.parse(this.props.location.search);
      const post = await getPostByIdService(parsed.postId);
      this.setState({ post });
    } catch (error) {
      this.props.history.replace('/notfound', { message: error.message });
    }
  }

  render() {
    const post: Post = this.state.post;
    return (
      <Container className={post == null ? 'fluid' : ''}>
        <Content className={post == null ? 'fluid' : ''}>
          <h3 className="blog-title" style={{ fontSize: 24 }}>
            Golbog
          </h3>
          {post == null ? (
            <div className="center-content">
              <Ripple />
            </div>
          ) : (
            <PostContent>
              <h1 className="title">{post.title}</h1>
              <p className="date">
                {post.formattedDate}
                <span className="separator">&#183;</span>
                <span className="author">{post.username}</span>
              </p>
              <MyEditor value={post.body} isEnableEditing={false} />
            </PostContent>
          )}
        </Content>
      </Container>
    );
  }
}

const PostContent = styled.div`
  h1.title {
    margin: 0;
    font-size: 32px;
    font-family: 'Oswald', sans-serif;
    font-weight: bold;
    letter-spacing: 1px;
  }

  .date {
    font-size: 90%;
    color: rgb(98, 98, 98);
    margin: 4px 0;
  }

  .separator {
    font-size: 18px;
    color: black;
    margin: 0 2px;
  }

  .author {
    color: #ff9900;
  }
`;
