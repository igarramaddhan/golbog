//@flow
import React, { Component } from 'react';
import PostList from '../components/PostList';
import Container from '../components/Container';
import Content from '../components/Content';
import Button from '../components/Button';
import { clearAuthToken } from '../services/client';
import { RouteComponentProps } from 'react-router-dom';

type Props = {};
type State = {};

export default class Dashboard extends Component<
  Props & RouteComponentProps,
  State
> {
  state = {};
  render() {
    return (
      <Container className="fluid">
        <Content className="fluid">
          <span
            style={{
              position: 'absolute',
              right: 32,
              top: 16,
              color: 'red',
              cursor: 'pointer'
            }}
            onClick={() => {
              clearAuthToken();
              this.props.history.push('/');
            }}
          >
            Logout
          </span>
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center'
            }}
          >
            <h1 className="blog-title">Dashboard</h1>
            <Button
              style={{ width: 'unset' }}
              text="Write Post"
              onClick={() => this.props.history.push('/write')}
            />
          </div>
          <PostList />
        </Content>
      </Container>
    );
  }
}
