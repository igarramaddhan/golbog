/// @flow
import React, { Component } from 'react';
import { getPostsService } from '../services/post';
import { Post } from '../libs/types';
import { RouteComponentProps } from 'react-router-dom';
import Container from '../components/Container';
import Content from '../components/Content';
import styled from 'styled-components';
import PostList from '../components/PostList';

type State = {
  posts: Post[] | null
};

type Props = {};

export default class Index extends Component<
  Props & RouteComponentProps,
  State
> {
  state = {
    posts: null
  };
  async componentDidMount() {
    try {
      const posts = await getPostsService();
      this.setState({ posts });
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    return (
      <Container className="fluid">
        <Content className="fluid">
          <h1 className="blog-title">Golbog</h1>
          <Bio>
            <p>Igar personal blog. I write everything that I like.</p>
          </Bio>
          <PostList />
        </Content>
      </Container>
    );
  }
}

const Bio = styled.div`
  margin: 0 0 56px 0;

  p {
    font-family: 'Lato', sans-serif;
    margin: 0;
    letter-spacing: 1px;
  }
`;
