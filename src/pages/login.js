import React, { useState } from 'react';
import styled from 'styled-components';
import { RouteComponentProps } from 'react-router-dom';
import Button from '../components/Button';
import Container from '../components/Container';
import Content from '../components/Content';
import Form, { Input, Error } from '../components/Form';
import { authClient, setAuthToken } from '../services/client';

const loginService = async (username, password) => {
  try {
    const response = await authClient.post('/login', { username, password });
    await setAuthToken(response.data.token);
    console.log(response.data);
  } catch (error) {
    throw error.response.data;
  }
};

const Login = (props: RouteComponentProps) => {
  const [ loading, setLoading ] = useState(false);
  const [ username, setUsername ] = useState('');
  const [ password, setPassword ] = useState('');
  const [ error, setError ] = useState('');
  return (
    <Container className="fluid">
      <Content className="fluid">
        <div className="center-content">
          <LoginContainer>
            <h2 className="blog-title" style={{ margin: 0 }}>
              LOGIN
            </h2>
            <Form>
              <label>Username</label>
              <Input
                required
                name="username"
                placeholder="Type your username"
                onChange={e => setUsername(e.target.value)}
              />
              <label>Password</label>
              <Input
                required
                name="password"
                placeholder="Type your password"
                type="password"
                onChange={e => setPassword(e.target.value)}
              />
              {error !== '' && <Error>{error}</Error>}
              <Button
                loading={loading}
                // disabled={username == '' || password == ''}
                onClick={async e => {
                  e.preventDefault();
                  setLoading(true);
                  setError('');
                  console.log(username, password);
                  if (username === '' || password === '') {
                    return setError('Fill all form');
                  }
                  try {
                    await loginService(username, password);
                    setLoading(false);
                    props.history.replace('/dashboard');
                  } catch (err) {
                    console.log(err);
                    setError(err.message);
                    setLoading(false);
                  }
                }}
                text="Login"
              />
            </Form>
          </LoginContainer>
        </div>
      </Content>
    </Container>
  );
};

const LoginContainer = styled.div`
  border-radius: 6px;
  box-shadow: 0 2px 15px 0 rgba(210, 214, 220, 0.5);
  max-width: 400px;
  overflow: hidden;
  background: rgba(252, 252, 252);
  padding: 16px;
  min-width: 380px;

  @media only screen and (max-width: 420px) {
    box-shadow: none;
    background: none;
    min-width: 100%;
  }
`;

export default Login;
