import React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import Container from '../components/Container';
import Content from '../components/Content';
type Props = {};

const NotFound = (props: Props & RouteComponentProps) => {
  const state = props.history.location.state;
  return (
    <Container className="fluid">
      <Content className="fluid">
        <h3 className="blog-title" style={{ fontSize: 24 }}>
          Golbog
        </h3>
        <div className="center-content">
          <h2>404</h2>
          <p>{state != null ? state.message : 'Page Not Found'}</p>
        </div>
      </Content>
    </Container>
  );
};

export default NotFound;
