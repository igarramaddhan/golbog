/**@flow*/

// $FlowFixMe
import React, { Component, Suspense, lazy } from 'react';
// import Text from './components/Text';
// import MyEditor from '../components/Editor';
const MyEditor = lazy(() =>
  import(/*webpackChunkName:"Editor"*/ '../components/Editor')
);

const Text = lazy(() => import('../components/Text'));

type Props = {};

type State = {
  status: string,
  value: string,
  selected: string
};

class MyComponent extends Component<Props, State> {
  state = {
    status: '',
    value: '',
    selected: ''
  };

  componentDidMount() {
    const local = localStorage.getItem('text');
    if (local != null) this.setState({ value: local });
  }

  render() {
    return (
      <div>
        <p>{this.state.status}</p>
        {this.state.status !== '' && (
          <Suspense fallback={<p>Loading ..</p>}>
            <Text text="Hello there haha" />
          </Suspense>
        )}
        <p>Nice to meet you</p>
        <button
          onClick={() =>
            this.setState({ status: 'ahahaha wow nicee wkwkwkwk lol' })
          }
        >
          change status
        </button>
        <select
          value={this.state.selected}
          onChange={e => this.setState({ selected: e.target.value })}
        >
          {[ 'a', 'b', 'c' ].map(s => (
            <option key={s} value={s}>
              {s}
            </option>
          ))}
        </select>
        <Suspense fallback={<p>Please wait loading editor...</p>}>
          <MyEditor
            isEnableEditing={true}
            value={this.state.value}
            onEditorTextChange={text => {
              this.setState({ value: text });
              localStorage.setItem('text', text);
            }}
          />
        </Suspense>
      </div>
    );
  }
}
export default MyComponent;
