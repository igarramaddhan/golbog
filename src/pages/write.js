//@flow
import React, { Component } from 'react';
import Container from '../components/Container';
import Content from '../components/Content';
import MyEditor from '../components/Editor';
import Button from '../components/Button';

type Props = {};
type State = {
  value: string,
  isPreview: boolean
};

export default class Write extends Component<Props, State> {
  state = {
    value: '',
    isPreview: true
  };

  onEditorTextChange = (text: string) => {
    this.setState({ value: text });
  };
  render() {
    return (
      <Container>
        <Content>
          <Button
            text="toggle preview"
            onClick={() =>
              this.setState(prev => {
                return { isPreview: !prev.isPreview };
              })
            }
          />
          <MyEditor
            value={this.state.value}
            isEnableEditing={this.state.isPreview}
            onEditorTextChange={this.onEditorTextChange}
          />
        </Content>
      </Container>
    );
  }
}
