//@flow
// $FlowFixMe
import React, { Component, ReactNode, ErrorInfo } from 'react';

type Props = {
  children: ReactNode
};
type State = {
  error: any,
  errorInfo: ErrorInfo
};

export default class ErrorBoundary extends Component<Props, State> {
  state = { error: null, errorInfo: null };
  componentDidCatch(error: Error, errorInfo: ErrorInfo) {
    this.setState({
      error: error,
      errorInfo: errorInfo
    });
  }

  render() {
    if (this.state.errorInfo) {
      return (
        <div>
          <h2>Something went wrong.</h2>
          <details style={{ whiteSpace: 'pre-wrap' }}>
            {this.state.error && this.state.error.toString()}
            <br />
            {this.state.errorInfo.componentStack}
          </details>
        </div>
      );
    }

    return this.props.children;
  }
}
