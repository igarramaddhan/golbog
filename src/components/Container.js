import styled from 'styled-components';

const Container = styled.div`
  flex: 1;
  background-color: #f2f2f2;
  font-family: 'Lato', sans-serif;

  &.fluid {
    display: flex;
    flex-direction: column;
  }

  .blog-title {
    color: black;
    font-size: 40px;
    position: relative;
    /* background: #f2f2f2; */
    font-family: 'Oswald', sans-serif;
    font-weight: bolder;
    letter-spacing: 4px;
  }
`;

export default Container;
