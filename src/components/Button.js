//@flow
import * as React from 'react';
import type { Node } from 'react';
import styled from 'styled-components';
import Loader from './Loader';

const ButtonContainer = styled.button`
  font-family: 'Oswald', sans-serif;
  font-weight: bold;
  outline: none;
  background-color: #ff9900;
  box-shadow: ${ props =>
    props.loading
      ? '0 0.5px 0.5px rgba(0, 0, 0, 0.4)'
      : '0 1px 4px rgba(0, 0, 0, 0.4)' };
  border: none;
  width: 100%;
  min-height: 48px;
  margin: 8px 0;
  cursor: pointer;
  border-radius: 8px;
  font-size: 18px;
  color: white;

  &:disabled {
    background-color: #e5e5e5;
    box-shadow: none;
  }

  div:nth-child(1) {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  p {
    margin: 0;
  }
`;

// $FlowFixMe
type Props = React.HTMLProps<HTMLButtonElement> & {
  children?: Node,
  loading?: boolean | false,
  text?: string
};

const Button = ({ loading, text, children, onClick, ...props }: Props) => {
  return (
    <ButtonContainer
      {...props}
      loading={loading}
      onClick={loading ? null : onClick}
    >
      {children ? children : <div>{loading ? <Loader /> : <p>{text}</p>}</div>}
    </ButtonContainer>
  );
};

export default Button;
