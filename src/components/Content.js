import styled from 'styled-components';

const Content = styled.div`
  max-width: 670px;
  margin: 0 auto;
  padding: 2.625rem 1.3125rem;

  &.fluid {
    flex: 1;
    display: flex;
    flex-direction: column;
    width: 670px;
  }

  .center-content {
    flex: 1;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    /* height: 300px; */
  }

  @media only screen and (max-width: 768px) {
    margin: 0;
    &.fluid {
      width: unset;
    }
  }
`;

export default Content;
