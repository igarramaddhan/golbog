import React from 'react';
import styled from 'styled-components';

const Form = styled.form`
  & > label {
    font-family: 'Oswald', sans-serif;
    margin: 0 0 0 8px;
    letter-spacing: 1px;
  }
`;

const Error = styled.span`
  font-family: 'Lato', sans-serif;
  margin: 8px;
  color: red;
`;

const InputContainer = styled.div`
  margin: 0 0 8px 0;

  input {
    border: none;
    outline: none;
    background-color: #e5e5e5;
    width: calc(100% - 16px);
    height: 32px;
    border-radius: 8px;
    padding: 8px;
    font-family: 'Lato', sans-serif;
    font-size: 18px;
  }
`;

type InputProps = React.HTMLProps<HTMLInputElement> & {
  error?: string
};

const Input = (props: InputProps) => {
  console.log('error', props.error);
  return (
    <InputContainer>
      <input {...props} />
      {props.error !== '' &&
        props.error !== null &&
        props.error !== undefined && <Error>{props.error}</Error>}
    </InputContainer>
  );
};

export { Input, Error };
export default Form;
