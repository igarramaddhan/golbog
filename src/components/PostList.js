// @flow
// $FlowFixMe
import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import Link from 'react-router-dom/Link';
import { Post } from '../libs/types';
import Ripple from './Ripple';
import { getPostsService } from '../services/post';

type State = {
  posts: Post[] | null
};

const PostList = () => {
  const [ posts, setPost ] = useState<State>(null);

  async function getPosts() {
    try {
      const dataPosts = await getPostsService();
      setPost(dataPosts);
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getPosts();
  }, []);

  return (
    <Container>
      {posts != null ? (
        posts.length === 0 ? (
          <div className="center">Oops, no content</div>
        ) : (
          <ul className="posts">
            {posts.map((post: Post) => {
              return (
                <li key={post.id} className="post">
                  <h3 className="title">
                    <Link className="text" to={`/post?postId=${ post.id }`}>
                      {post.title}
                    </Link>
                  </h3>
                  <small className="date">
                    {post.formattedDate}
                    <span className="separator">&#183;</span>
                    <span className="author">{post.username}</span>
                  </small>
                  <p className="body">{post.shortContent}</p>
                </li>
              );
            })}
          </ul>
        )
      ) : (
        <div className="center">
          <Ripple />
        </div>
      )}
    </Container>
  );
};

export default PostList;

const Container = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;

  .center {
    flex: 1;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }

  ul.posts {
    padding: 0;
    font-family: 'Lato', sans-serif;

    li.post {
      list-style-type: none;
      margin-bottom: 16px;
    }

    .title {
      font-size: 1.4427rem;
      margin: 4px 0;

      .text {
        text-decoration: none;
        color: inherit;
      }
    }

    .date {
      font-size: 80%;
      color: rgb(98, 98, 98);
      margin: 4px 0;
    }

    .separator {
      font-size: 18px;
      color: black;
      margin: 0 2px;
    }

    .author {
      color: #ff9900;
    }

    .body {
      letter-spacing: normal;
      font-size: 100%;
      margin: 4px 0;
      line-height: 1.75;
      text-overflow: ellipsis;
      white-space: nowrap;
      overflow: hidden;
    }
  }
`;
