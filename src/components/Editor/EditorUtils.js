// @flow
import React from 'react';
import { Rule } from 'slate-html-serializer';
import { renderMark, renderNode } from './renderer';
import { onKeyDown } from './keyEvent';

const BLOCK_TAGS = {
  blockquote: 'quote',
  p: 'paragraph',
  h1: 'heading-one',
  h2: 'heading-two',
  h3: 'heading-three',
  h4: 'heading-four',
  h5: 'heading-five',
  h6: 'heading-six',
  ul: 'bulleted-list',
  ol: 'ordered-list',
  li: 'list-item',
  pre: 'code-block',
  code: 'code-block-item',
  br: 'break',
  hr: 'hr'
};

const MARK_TAGS = {
  em: 'italic',
  strong: 'bold',
  u: 'underline',
  code: 'code'
};

const rules: Rule[] = [
  {
    deserialize(el, next) {
      const type = BLOCK_TAGS[ el.tagName.toLowerCase() ];
      if (type) {
        if (el.getAttribute('class') == 'single') {
          return;
        }
        return {
          object: 'block',
          type: type,
          data: {
            className: el.getAttribute('class')
          },
          nodes: next(el.childNodes)
        };
      }
    },
    serialize(obj, children) {
      if (obj.object == 'block') {
        switch (obj.type) {
          case 'paragraph':
            return <p className={obj.data.get('className')}>{children}</p>;
          case 'quote':
            return <blockquote>{children}</blockquote>;
          case 'heading-one':
            return <h1>{children}</h1>;
          case 'heading-two':
            return <h2>{children}</h2>;
          case 'heading-three':
            return <h3>{children}</h3>;
          case 'heading-four':
            return <h4>{children}</h4>;
          case 'heading-five':
            return <h5>{children}</h5>;
          case 'heading-six':
            return <h6>{children}</h6>;
          case 'list-item':
          case 'ordered-list-item':
            return <li>{children}</li>;
          case 'bulleted-list':
            return <ul>{children}</ul>;
          case 'ordered-list':
            return <ol>{children}</ol>;
          case 'code-block':
            return <pre>{children}</pre>;
          case 'code-block-item':
            return <code>{children}</code>;
          case 'break':
            return <br />;
          case 'hr':
            return <hr />;
        }
      }
    }
  },
  // Add a new rule that handles marks...
  {
    deserialize(el, next) {
      const type = MARK_TAGS[ el.tagName.toLowerCase() ];
      if (type) {
        return {
          object: 'mark',
          type: type,
          nodes: next(el.childNodes),
          data: {
            className: el.getAttribute('class')
          }
        };
      }
    },
    serialize(obj, children) {
      if (obj.object == 'mark') {
        switch (obj.type) {
          case 'bold':
            return <strong>{children}</strong>;
          case 'italic':
            return <em>{children}</em>;
          case 'underline':
            return <u>{children}</u>;
          case 'code':
            return (
              <code className='single'>{children}</code>
            );
        }
      }
    }
  }
];

function MarkHotkey(options: { key: string, type: string }) {
  const { type, key } = options;
  // Return our "plugin" object, containing the `onKeyDown` handler.
  return {
    // $FlowFixMe
    onKeyDown(event: React.ChangeEvent<HTMLInputElement>, editor, next) {
      // If it doesn't match our `key`, let other plugins handle it.
      if (!event.ctrlKey || event.key != key) return next();
      // Prevent the default characters from being inserted.
      event.preventDefault();
      // Toggle the mark `type`.
      const isBlockTags = Object.keys(BLOCK_TAGS).some(
        valueKey => BLOCK_TAGS[ valueKey ] === type
      );
      if (isBlockTags) {
        const isCode = editor.value.blocks.some(block => block.type == type);
        // Toggle the block type depending on `isCode`.
        editor.setBlocks(isCode ? 'paragraph' : type);
        // editor.insertBlock(isCode ? 'paragraph' : type);
      } else {
        editor.toggleMark(type);
      }
    }
  };
}

export { renderMark, rules, renderNode, MarkHotkey, onKeyDown };
