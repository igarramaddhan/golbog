// @flow
import React from 'react';
import { Editor, RenderNodeProps, RenderMarkProps } from 'slate-react';
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter';
import { tomorrow } from 'react-syntax-highlighter/dist/styles/prism';
import styled from 'styled-components';

const CodeContainer = styled.div`
  pre {
    border-radius: 8px;
  }
`;

const Code = (props: { children: string, attributes: any }) => {
  return (
    <CodeContainer>
      <SyntaxHighlighter
        {...props.attributes}
        language="javascript"
        style={tomorrow}
        showLineNumbers={true}
        wrapLines={false}
      >
        {`${ props.children }`}
      </SyntaxHighlighter>
    </CodeContainer>
  );
};

const SingleCode = (props: { children: string, attributes: any }) => {
  return (
    <code
      style={{ background: '#ff9900', color: 'white', padding: '2px' }}
      {...props.attributes}
    >
      {props.children}
    </code>
  );
};

function renderMark(props: RenderMarkProps, editor: Editor, next: () => any) {
  const { mark, attributes } = props;
  switch (mark.type) {
    case 'bold':
      return <strong {...attributes}>{props.children}</strong>;
    case 'italic':
      return <em {...attributes}>{props.children}</em>;
    case 'underline':
      return <u {...attributes}>{props.children}</u>;
    case 'code':
      return <SingleCode {...props.attributes}>{props.children}</SingleCode>;
    default:
      return next();
  }
}

function CodeBlock(props: RenderNodeProps) {
  return (
    <div style={{ position: 'relative' }}>
      <pre>
        <code {...props.attributes}>{props.children}</code>
      </pre>
    </div>
  );
}

function CodeBlockItem(props: RenderNodeProps) {
  return <div {...props.attributes}>{props.children}</div>;
}

function renderNode(props: RenderNodeProps, editor: Editor, next: () => any) {
  switch (props.node.type) {
    case 'paragraph':
      return (
        <p {...props.attributes} className={props.node.data.get('className')}>
          {props.children}
        </p>
      );
    case 'quote':
      return <blockquote {...props.attributes}>{props.children}</blockquote>;
    case 'bulleted-list':
      return <ul {...props.attributes}>{props.children}</ul>;
    case 'ordered-list':
      return <ol {...props.attributes}>{props.children}</ol>;
    case 'heading-one':
      return <h1 {...props.attributes}>{props.children}</h1>;
    case 'heading-two':
      return <h2 {...props.attributes}>{props.children}</h2>;
    case 'heading-three':
      return <h3 {...props.attributes}>{props.children}</h3>;
    case 'heading-four':
      return <h4 {...props.attributes}>{props.children}</h4>;
    case 'heading-five':
      return <h5 {...props.attributes}>{props.children}</h5>;
    case 'heading-six':
      return <h6 {...props.attributes}>{props.children}</h6>;
    case 'list-item':
    case 'ordered-list-item':
      return <li {...props.attributes}>{props.children}</li>;
    case 'code-block':
      return editor.readOnly ? (
        <Code {...props.attributes}>
          {props.children.reduce((a, b) => `${ a }${ b.props.node.text }\n`, '')}
        </Code>
      ) : (
        <CodeBlock {...props.attributes}>{props.children}</CodeBlock>
      );

    case 'code-block-item':
      return !editor.readOnly ? (
        <CodeBlockItem {...props.attributes}>{props.children}</CodeBlockItem>
      ) : (
        next()
      );
    case 'hr':
      return <hr {...props.attributes} />;
    default:
      return next();
  }
}

export { renderMark, renderNode };
