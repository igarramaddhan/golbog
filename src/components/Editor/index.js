// @flow
import React, { Component } from 'react';
import { Editor } from 'slate-react';
import Html from 'slate-html-serializer';
import { Value } from 'slate';
import {
  renderMark,
  rules,
  renderNode,
  MarkHotkey,
  onKeyDown
} from './EditorUtils';
// eslint-disable-next-line css-modules/no-unused-class

import AutoReplace from 'slate-auto-replace';
import styled from 'styled-components';

const html = new Html({ rules });
const plugins = [
  MarkHotkey({ key: 'b', type: 'bold' }),
  MarkHotkey({ key: 'i', type: 'italic' }),
  MarkHotkey({ key: 'u', type: 'underline' }),
  MarkHotkey({ key: "'", type: 'quote' }),
  MarkHotkey({ key: '`', type: 'code' }),
  AutoReplace({
    trigger: 'space',
    before: /^(>)$/,
    change: change => {
      if (change.value.startBlock.type != 'code-block-item')
        return change.setBlocks({ type: 'quote' });
      else return change.insertText('> ');
    }
  }),
  AutoReplace({
    trigger: 'space',
    before: /^[-*+]$/,
    change: change => {
      if (change.value.startBlock.type != 'code-block-item')
        return change
          .setBlocks({ type: 'list-item' })
          .wrapBlock('bulleted-list')
          .deleteBackward(1);
      else return change.insertText(' ');
    }
  }),
  AutoReplace({
    trigger: 'space',
    before: /1./,
    change: change => {
      if (change.value.startBlock.type != 'code-block-item')
        return change
          .setBlocks({ type: 'ordered-list-item' })
          .wrapBlock('ordered-list')
          .deleteBackward(2);
      else return change.insertText(' ');
    }
  }),
  AutoReplace({
    trigger: 'space',
    before: /^(`{3})$/,
    change: change => {
      if (change.value.startBlock.type != 'code-block-item')
        return change
          .setBlocks({ type: 'code-block-item' })
          .wrapBlock('code-block');
      else return change.insertText(' ');
    }
  }),
  AutoReplace({
    trigger: 'space',
    before: /^(#{1,6})$/,
    change: (change, event, matches) => {
      const [ hashes ] = matches.before;
      if (change.value.startBlock.type != 'code-block-item') {
        let level;
        switch (hashes.length) {
          case 1:
            level = 'one';
            break;
          case 2:
            level = 'two';
            break;
          case 3:
            level = 'three';
            break;
          case 4:
            level = 'four';
            break;
          case 5:
            level = 'five';
            break;
          case 6:
            level = 'six';
            break;
          default:
            level = 'one';
            break;
        }
        change.setBlocks({ type: `heading-${ level }` });
      } else return change.insertText(`${ hashes } `);
    }
  }),
  AutoReplace({
    trigger: 'space',
    before: /`.*`/,
    change: (change: Editor, event, matches) => {
      if (change.value.startBlock.type != 'code-block-item') {
        const raw = matches.before[0];
        const text = raw.replace(/`/g, '');
        const currentNode = change.value.startBlock;
        change.removeTextByKey(
          currentNode.getFirstText().key,
          currentNode.text.replace(raw, '').length,
          raw.length
        );
        change
          .toggleMark('code')
          .insertText(text)
          .toggleMark('code')
          .insertText(' ');
      } else change.insertText(' ');
    }
  })
];

type Props = {
  value: string,
  isEnableEditing: boolean,
  onEditorTextChange: (text: string) => void
};

type State = {
  isEditorReady: boolean,
  value: Value
};

class MyEditor extends Component<Props, State> {
  static defaultProps = {
    isEnableEditing: true,
    onEditorTextChange: () => {}
  };

  static getDerivedStateFromProps(props: Props, state: State) {
    let obj = null;
    if (state.isEditorReady) {
      // if (!props.isEnableEditing) {
      obj = {
        value: html.deserialize(props.value)
      };
      if (html.serialize(obj.value) != html.serialize(state.value)) {
        return obj;
      }
      // }
    }
    return null;
  }

  state: State = {
    value: null,
    isEditorReady: false
  };

  componentDidMount() {
    const initialValue = '';
    this.setState({
      isEditorReady: true,
      value: html.deserialize(initialValue)
    });
  }

  onChange = ({ value }: { value: Value }) => {
    // if (value.document != this.state.value.document) {
    //   const content = html.serialize(value);
    //   this.props.onEditorTextChange(content);
    // }
    if (value.document != this.state.value.document) {
      const content = html.serialize(value);
      this.props.onEditorTextChange(content);
    }
    this.setState({ value });
  };

  render() {
    return this.state.isEditorReady ? (
      <StyledEditor
        placeholder="Write something"
        value={this.state.value}
        onChange={this.onChange}
        renderMark={renderMark}
        renderNode={renderNode}
        plugins={plugins}
        onKeyDown={onKeyDown}
        readOnly={!this.props.isEnableEditing}
      />
    ) : (
      <h4>Loading editor...</h4>
    );
  }
}

export default MyEditor;

const StyledEditor = styled(Editor)`
  line-height: 1.58;

  blockquote {
    color: grey;
    border-left: 3px #ff9900 solid;
    padding-left: 8px;
    margin-left: -13px;
    font-style: italic;
  }

  /* code {
    background-color: #ff9900;
    color: white;
    padding: 2px;
  } */

  pre {
    background: #e5e5e5;
    padding: 8px;

    & > code {
      padding: 0;
      font-size: inherit;
      color: black;
    }
  }

  @media only screen and (max-width: 768px) {
    blockquote {
      margin: 16px 0;
    }
  }
`;
