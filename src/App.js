// @flow
// $FlowFixMe
import React, { lazy, Suspense, useState, useEffect } from 'react';
import {
  RouteProps,
  BrowserRouter,
  Route,
  Switch,
  Redirect
} from 'react-router-dom';
import ErrorBoundary from './components/ErrorBoundary';
import Ripple from './components/Ripple';
import { hot } from 'react-hot-loader/root';

import NotFound from './pages/NotFound';
const Index = lazy(() => import('./pages/index'));
const Home = lazy(() => import('./pages/home'));
const Login = lazy(() => import('./pages/login'));
const About = () => <div> About Screen </div>;
const Post = lazy(() => import('./pages/post'));
const Dashboard = lazy(() => import('./pages/dashboard'));
const Write = lazy(() => import('./pages/write'));

class App extends React.Component<{}> {
  render() {
    return (
      <ErrorBoundary>
        <BrowserRouter>
          <Suspense
            fallback={
              <div
                style={{
                  flex: 1,
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  background: '#f2f2f2'
                }}
              >
                <Ripple />
              </div>
            }
          >
            <Switch>
              <Route exact path="/" render={props => <Index {...props} />} />
              <Route exact path="/home" render={props => <Home {...props} />} />
              <Route
                exact
                path="/login"
                render={props => <Login {...props} />}
              />
              <PrivateRoute
                exact
                path="/home/about"
                component={About}
                // render={props => <About {...props} />}
              />
              <Route path="/post" render={props => <Post {...props} />} />
              <PrivateRoute path="/dashboard" component={Dashboard} />
              <PrivateRoute path="/write" component={Write} />
              {/* <Route path="*" render={props => <NotFound {...props} />} /> */}
              <Route component={NotFound} />
            </Switch>
          </Suspense>
        </BrowserRouter>
      </ErrorBoundary>
    );
  }
}

export default hot(App);

function PrivateRoute({ component: Component, ...rest }: RouteProps) {
  const [ loading, setLoading ] = useState(true);
  const [ token, setToken ] = useState(null);

  function getToken() {
    const data = localStorage.getItem('token');
    setToken(data);
    setLoading(false);
  }

  useEffect(() => {
    getToken();
  }, []);

  return loading ? (
    <div>Please wait...</div>
  ) : (
    <Route
      {...rest}
      render={props =>
        token ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: props.location }
            }}
          />
        )
      }
    />
  );
}
