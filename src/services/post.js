//@flow
import { client } from './client';
import { Post } from '../libs/types';
import moment from 'moment';

function convertFromRaw(post: Post): Post {
  const cleanBody = post.body.replace(/<\/?[^>]+(>|$)/g, '');
  return {
    ...post,
    formattedDate: moment(post.createdAt)
      .format('MMMM DD, YYYY')
      .toString(),
    shortContent:
      cleanBody.length > 56 ? cleanBody.substr(0, 56) + '...' : cleanBody
  };
}

export const getPostsService = async (): Promise<Array<Post>> => {
  try {
    const response = await client.get<{ posts: Array<Post> }>('/post');
    const posts = response.data.posts.map((post: Post) => {
      return convertFromRaw(post);
    });
    return posts;
  } catch (error) {
    throw error.response.data;
  }
};

export const getPostByIdService = async (id: number): Promise<Post> => {
  try {
    const response = await client.get<{ post: Post }>(`/post/${ id }`);
    const post = response.data.post;
    if (post != null) return convertFromRaw(response.data.post);
    throw new Error('Post Not Found');
  } catch (error) {
    throw error.response.data;
  }
};

export const createPostService = async (
  title: string,
  body: string
): Promise<Post> => {
  try {
    const response = await client.post<Post>('/post', {
      title,
      body
    });
    return response.data.post;
  } catch (error) {
    throw error.response.data;
  }
};
