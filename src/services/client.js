import Axios from 'axios';
const BASE_URL = 'https://api.ynrk.tk/blog/api';
// const BASE_URL = 'http://localhost:3000/api';

// authClient for login and register (don't need access token)
export const authClient = Axios.create({
  baseURL: BASE_URL,
  headers: { 'Content-Type': 'application/json', Accept: 'application/json' }
});

export const client = Axios.create({
  baseURL: BASE_URL,
  headers: { 'Content-Type': 'application/json', Accept: 'application/json' }
});

export const readLocalTOken = async () => {
  const token = await localStorage.getItem('token');
  client.defaults.headers.common['Authorization'] = `Bearer ${ token }`;
  return token;
};

export const setAuthToken = async token => {
  await localStorage.setItem('token', token);
  client.defaults.headers.common['Authorization'] = `Bearer ${ token }`;
};

export const clearAuthToken = async () => {
  await localStorage.clear();
};
