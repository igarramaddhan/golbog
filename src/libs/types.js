export type Post = {
  body: string,
  createdAt: string,
  id: number,
  title: string,
  updatedAt: string,
  username: string,
  formattedDate: string,
  shortContent: string
};
