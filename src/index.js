import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

const render = (Container): React.Component => {
  ReactDOM.render(<Container />, document.querySelector('#root'));
};

render(App);

if (module.hot) {
  module.hot.accept('./App', function() {
    render(require('./App').default);
  });
}
