const path = require('path');
// const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = {
  devtool: 'inline-source-map',
  mode: 'development',
  performance: {
    hints: false
  },
  entry: {
    app: [
      'babel-polyfill',
      'react-hot-loader/patch',
      path.resolve(__dirname, 'src/index.js')
    ]
  },
  output: {
    filename: '[name].bundle.js',
    chunkFilename: '[name].bundle.js'
  },
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: 3300,
    watchContentBase: true,
    progress: false,
    hot: true,
    historyApiFallback: true
  },
  plugins: [
    new CleanWebpackPlugin([ 'dist' ]),
    new HtmlWebpackPlugin({
      template: './src/index.html'
    })
  ],
  optimization: {
    minimizer: [
      new TerserPlugin({
        parallel: 4,
        cache: path.join(__dirname, '.cache'),
        terserOptions: {
          output: {
            comments: false
          }
        }
      })
    ],
    namedChunks: true,
    splitChunks: {
      chunks: 'async',
      minSize: 30000,
      maxSize: 0,
      minChunks: 1,
      maxAsyncRequests: 5,
      maxInitialRequests: Infinity,
      automaticNameDelimiter: '~',
      name: true,
      cacheGroups: {
        vendor: {
          test: /node_modules/,
          chunks: 'initial',
          name: 'vendor',
          enforce: true
        },
        momentVendor: {
          test: /[\\/]node_modules[\\/](moment)[\\/]/,
          name: 'momentvendor',
          chunks: 'all'
        },
        lodashVendor: {
          test: /[\\/]node_modules[\\/](lodash)[\\/]/,
          name: 'lodashVendor',
          chunks: 'all'
        },
        // reactVendor: {
        //   test: /[\\/]node_modules[\\/](react|react-dom)[\\/]/,
        //   name: 'reactvendor',
        //   chunks: 'all'
        // },
        // slateVendor: {
        //   test: /[\\/]node_modules[\\/](slate-react|slate)[\\/]/,
        //   name: 'slatevendor',
        //   chunks: 'all'
        // },
        // slateHtmlSerializerVendor: {
        //   test: /[\\/]node_modules[\\/](slate-html-serializer)[\\/]/,
        //   name: 'slatehtmlserializervendor',
        //   chunks: 'all'
        // },
        default: {
          minChunks: 2,
          priority: -20,
          reuseExistingChunk: true
        }
      }
    }
  },
  module: {
    rules: [
      {
        enforce: 'pre',
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'eslint-loader',
        options: {
          failOnError: true
        }
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: true
            }
          }
        ]
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [ 'file-loader' ]
      }
    ]
  }
};
